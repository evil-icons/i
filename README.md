##### evil icons by Alexander Madyankin & Roman Shamin

Full ownership of *evil icons* goes to Alexander Madyankin & Roman Shamin.

- **Website:** [evil-icons.io](https://evil-icons.io)
- **GitHub:** [github.com/evil-icons/evil-icons](https://github.com/evil-icons/evil-icons)
- **Icons list:** [icones.js.org/collection/ei](https://icones.js.org/collection/ei)
- **Version:** v1.10.1 (2018-06-06)

**How to use:**

Include this after `<head>` in your site:
```html
<link href="https://evil-icons.gitlab.io/i/icons.css" rel="stylesheet">
```

To place an icon, insert the following code where you want your icon to be:
```html
<i class="typicons" icon-name="arrow-right"></i>
```

Replace `arrow-right` (aka the content inside `icon-name=""`) with the name of the icon of your choosing.
